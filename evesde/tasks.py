import csv
import hashlib
import logging
import re
import tempfile
from bz2 import decompress
from datetime import datetime
from email.utils import mktime_tz, parsedate_tz
from urllib.parse import urljoin

import pytz
import requests
from celery import shared_task

from django.contrib.auth.models import Permission, User
from django.db import IntegrityError, transaction
from django.db.models import Q

from allianceauth.notifications import notify

from .app_settings import EVESDE_DOWNLOAD_SERVER_URL
from .models import *  # noqa
from .models import UpdateStatus, get_evesde_mappings
from .utils import LoggerAddTag, make_logger_prefix

logger = LoggerAddTag(logging.getLogger(__name__), __package__)


# report modes
REPORT_MODE_FULL = "full"
REPORT_MODE_EVENTS = "events"
REPORT_MODE_ERRORS = "errors"
REPORT_MODE_QUIET = "quiet"


def _send_report(title: str, text: str, level="info"):
    """send report to everyone who has the logging_notification permission

    Args:
        title: message title
        text: message text
        level: alert level of message (optional)
    """
    try:
        perm = Permission.objects.get(codename="logging_notifications")

        users = User.objects.filter(
            Q(groups__permissions=perm)
            | Q(user_permissions=perm)
            | Q(is_superuser=True)
        ).distinct()

        for user in users:
            notify(user=user, title=title, message=text, level=level)

    except Permission.DoesNotExist:
        pass


def _fetch_csv_file_from_server(table_name: str, download_url: str, addTag):
    """Returns an SDE table in CSV format from the download server

    Args:
        table_name: name of the SDE table to fetch
        download_url: base url to use for all downloads
        addTag: logger tag
    Returns:
        data_hash of the decompressed csv data
        last modified timestamp of the csv file
        file pointer to decompressed csv file

    """
    # fetch the file from the FTP server
    logger.info(
        addTag("Started downloading table {} from {}".format(table_name, download_url))
    )
    response = requests.get(urljoin(download_url, table_name + ".csv.bz2"))
    response.raise_for_status()

    # get date
    timestamp = mktime_tz(parsedate_tz(response.headers["last-modified"]))
    utc_dt = datetime.utcfromtimestamp(timestamp)
    last_modified = pytz.utc.localize(utc_dt)

    # decompress data into CSV string
    file_bz2 = response.content
    csv_string = decompress(file_bz2).decode("utf-8")

    # calc MD5 data_hash on csv_string
    data_hash = hashlib.md5(csv_string.encode("utf-8")).hexdigest()

    # store csv_string as temp file for follow up processing
    f_csv = tempfile.TemporaryFile(mode="w+", encoding="utf-8", newline="")
    f_csv.write(csv_string)
    f_csv.seek(0)

    # return downloaded csv file
    logger.info(
        addTag(
            "Completed downloading table {} from {}".format(table_name, download_url)
        )
    )
    return data_hash, last_modified, f_csv


def _convert_value(k, v, none_mappings: dict = None, field_mappings: dict = None):
    """converts the value by detecting None and applying type conversions

    None: converts 'None' to None by default
    or uses custom string to detect None if specified in none_mappings

    Fields: conversion of field to a type if defined in field_mappings

    All other values will be passed through
    """
    # convert to None
    if none_mappings is None and v == "None":
        v = None
    elif none_mappings is not None and k in none_mappings and v == none_mappings[k]:
        v = None
    if v is not None and field_mappings is not None:
        if k in field_mappings:
            field_type = field_mappings[k]
            if field_type == "bool":
                v = True if v == "1" else False
    return v


def _convert_camel_to_snake(name: str) -> str:
    """converts a name in camel case to snake case"""
    s1 = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
    return re.sub("([a-z0-9])([A-Z])", r"\1_\2", s1).lower()


def _patch_column(name: str, name_mappings: dict) -> str:
    """replaces column names if they have an entry in name_mappings dict"""
    if name in name_mappings:
        return name_mappings[name]
    else:
        return name


@shared_task
def run_update(
    model_to_update: str = None,
    force_update: bool = False,
    download_url: str = None,
    report_mode: str = REPORT_MODE_FULL,
):
    """perform updates for selected models

    when called with no params will update the standard set up models only

    Args:
        model_to_update: only update model with this name (e.g. 'EveRace')
        force_update: will perform full update even if hashes matches
        download_url: user provided download URL instead of the one in settings
        report_mode: defines when reports are sent to admin.
            'full': always report result of update process, report errors
            'events': report only if a model was updated and on errors
            'errors': only report errors
            'quiet': disable all reports

    """

    mapped_models_by_name = [x["name"] for x in get_evesde_mappings()]
    addTag = make_logger_prefix("no_model")
    try:
        # determine list of models to update
        if model_to_update is not None:
            if model_to_update not in mapped_models_by_name:
                logger.error("Unknown model: {}".format(model_to_update))
                return
            else:
                models_selected = [model_to_update]
        else:
            models_selected = mapped_models_by_name

        # set download url
        if download_url is None:
            download_url = EVESDE_DOWNLOAD_SERVER_URL

        # adjust report mode
        if report_mode not in [
            REPORT_MODE_FULL,
            REPORT_MODE_EVENTS,
            REPORT_MODE_ERRORS,
            REPORT_MODE_QUIET,
        ]:
            report_mode = REPORT_MODE_FULL

        # start update
        logger.info(
            "Started {} update of Eve Static Data for: {} "
            'with report mode "{}"'.format(
                "forced" if force_update else "regular", models_selected, report_mode
            )
        )
        mappings_selected = [
            x for x in get_evesde_mappings() if x["name"] in models_selected
        ]
        models_updated_status = dict()
        # models_count = len(mappings_selected)
        for mapping in mappings_selected:
            ModelClass = mapping["model"]
            model_name = mapping["name"]
            addTag = make_logger_prefix(model_name)
            data_hash, last_modified, f_csv = _fetch_csv_file_from_server(
                mapping["table"], download_url, addTag
            )

            try:
                status = UpdateStatus.objects.get(pk=model_name)
            except UpdateStatus.DoesNotExist:
                status = None

            if (
                status is not None
                and status.data_hash == data_hash
                and force_update is False
            ):
                logger.info(
                    addTag(
                        "Skipping update for this model since"
                        + " SDE table {} has not changed.".format(mapping["table"])
                    )
                )
                models_updated_status[model_name] = False

            else:
                # Insert data into table
                with transaction.atomic():
                    logger.info(addTag("started storing updates in DB"))
                    rows_processed_count = 0
                    rows_added_count = 0
                    for row in csv.DictReader(f_csv, delimiter=","):
                        # patch column names in input table if defined
                        if "name_mappings" in mapping:
                            row = {
                                _patch_column(k, mapping["name_mappings"]): v
                                for k, v in row.items()
                            }

                        # convert column names to snake case
                        # and apply field mappings
                        # and convert to None
                        if "none_mappings" in mapping:
                            none_mappings = mapping["none_mappings"]
                        else:
                            none_mappings = None
                        if "field_mappings" in mapping:
                            field_mappings = mapping["field_mappings"]
                        else:
                            field_mappings = None

                        object_full = {
                            _convert_camel_to_snake(k): _convert_value(
                                k, v, none_mappings, field_mappings
                            )
                            for k, v in row.items()
                        }

                        # generate function call as dict
                        if "pk_columns" in mapping:
                            pk_columns = mapping["pk_columns"]
                        else:
                            pk_columns = [ModelClass._meta.pk.column]
                        fargs = {k: object_full[k] for k in pk_columns}
                        fargs["defaults"] = {
                            k: v for k, v in object_full.items() if k not in pk_columns
                        }

                        # logger.info('object to create: {}'.format(fargs))

                        try:
                            obj, created = ModelClass.objects.update_or_create(**fargs)
                        except IntegrityError as error:
                            # ignore rows which do not have a match
                            # in the parent table, which are likely data errors
                            logger.info(
                                addTag(
                                    "failed to add row: {} ".format(fargs)
                                    + "due to IntegrityError: {}".format(error)
                                )
                            )

                        rows_processed_count += 1
                        if created:
                            rows_added_count += 1

                    f_csv.close()
                    logger.info(
                        addTag(
                            "Update done: {:,} rows updated, {:,} rows added".format(
                                rows_processed_count - rows_added_count,
                                rows_added_count,
                            )
                        )
                    )

                    UpdateStatus.objects.update_or_create(
                        model_name=model_name,
                        defaults={
                            "last_modified": last_modified,
                            "data_hash": data_hash,
                            "last_updated": pytz.utc.localize(datetime.utcnow()),
                        },
                    )
                    models_updated_status[model_name] = True

        # generate and send status report
        msg = (
            "Update process completed successfully. "
            + "Here is the result for each model:\n\n"
        )

        has_update = False
        for model_name, is_updated in models_updated_status.items():
            status_str = "UPDATED" if is_updated else "skipped"
            msg += "- {}: {}\n".format(model_name, status_str)
            if is_updated:
                has_update = True

        logger.info(msg.replace("\n", " "))
        if report_mode == REPORT_MODE_FULL or (
            report_mode == REPORT_MODE_EVENTS and has_update
        ):
            _send_report("Eve Static Data: update process completed", msg, "success")

    except Exception as err:
        logger.error(
            addTag("Unexpected error while updating Eve static data: {}".format(err))
        )
        if report_mode != REPORT_MODE_QUIET:
            msg = "Failed to update Eve status data: {}".format(err.__class__.__name__)
            _send_report("Eve Static Data: update process failed", msg, "warning")
            raise RuntimeError(msg)


@shared_task
def purge_all_data(i_am_sure: bool = False):
    """will remove data from all tables

    Args:
    - i_am_sure: set to True to purge tables
    """
    if not i_am_sure:
        logger.info("purge not started")
    else:
        logger.info("Started purging all data")
        try:
            with transaction.atomic():
                for mapping in reversed(get_evesde_mappings()):
                    ModelClass = mapping["model"]
                    model_name = ModelClass.__name__
                    logger.info("{}: purging data".format(model_name))
                    ModelClass.objects.all().delete()
                    try:
                        UpdateStatus.objects.get(pk=model_name).delete()
                    except UpdateStatus.DoesNotExist:
                        pass

        except Exception as err:
            logger.error("Failed to purge data: {}", err)
            _send_report(
                "Eve Static Data: purge_all_data failed",
                "Purge all data failed. Error was: {}".format(err.__class__.__name__),
                "warning",
            )

        else:
            logger.info("Completed purging of all data")
            _send_report(
                "Eve Static Data: purge_all_data completed",
                "All Eve statis data was purged successfully",
            )
