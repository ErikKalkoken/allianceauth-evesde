import math
import sys

from django.db import models
from django.utils.translation import gettext_lazy as _

# models for the most common Eve Online entities defined in the SDE
#
# Naming notation:
# -----------------
# Classes are in Pascal Case. Name is always "Eve" plus name of entity.
#
# Properties are the lowercase with underscores representation
# of the field name in the SDE tables, e.g. SolarSystemID becomes solar_system_id
#
# Foreign keys are named after the model they link to in lower case
# with underscores but without the leading eve
# e.g. constellation is FK to EveConstellation in EveSolarSystems
#
# Modelling rules
# ------------------
# - original PK field must to be defined as PK field in the model
# - all entities should define a __str__ method
# - fields that are FKs, but not modelled must to have an index (db_index=True)
# - docstring must include the original SDE table name for reference
# - fields are in same order than in SDE tables
# - list classes in alphabetical order
# - each model needs a SDE mapping (see below)
#
# SDE Mappings
# --------------
# SDE mapping are defined by adding the class EveSdeMapping to a model
#
# Properties MANDATORY:
#   table:  name of SDE table (=filename without extension) (MANDATORY)
#   order:  order number the table shall be imported
#           important for complying with FK constraints
#
# Properties Optional:
#   name_mappings: mapping of column names in CSV data
#   field_mappings: mapping of fields to type (e.g. mandatory for bool!)
#   none_mappings: overrides default identifier to detect None per column
#   pk_columns: define the columns of the functional PK for multi-column PKs
#
# ToDos:
# - Add invMarketGroup
# - Add invFlags


def get_evesde_mappings() -> list:
    """get the sorted mappings of all Eve SDE models"""
    mappings = list()
    for x in [
        eval(x)
        for x in dir(sys.modules[__name__])
        if x[:1] != "_" and hasattr(eval(x), "EveSdeMapping")
    ]:
        eve_mapping = x.EveSdeMapping
        mapping = {
            "model": x,
            "name": x.__name__,
            "table": eve_mapping.table,
            "order": eve_mapping.order,
        }
        if hasattr(eve_mapping, "name_mappings"):
            mapping["name_mappings"] = eve_mapping.name_mappings

        if hasattr(eve_mapping, "field_mappings"):
            mapping["field_mappings"] = eve_mapping.field_mappings

        if hasattr(eve_mapping, "pk_columns"):
            mapping["pk_columns"] = eve_mapping.pk_columns

        if hasattr(eve_mapping, "none_mappings"):
            mapping["none_mappings"] = eve_mapping.none_mappings

        mappings.append(mapping)

    return sorted(mappings, key=lambda x: x["order"])


class UpdateStatus(models.Model):
    """Update status of all models"""

    model_name = models.CharField(max_length=30, primary_key=True)
    last_modified = models.DateTimeField(
        help_text="datetime the sde data was last modified on the server"
    )
    data_hash = models.CharField(
        max_length=32,
        null=True,
        default=None,
        help_text="hash of the sde data for identifying changes",
    )
    last_updated = models.DateTimeField(
        help_text="datetime the local copy of the sde data was last updated"
    )

    def __str__(self):
        return self.model_name

    class Meta:
        verbose_name_plural = _("update status")


class EveCategory(models.Model):
    category_id = models.IntegerField(primary_key=True)
    category_name = models.CharField(max_length=100, null=True, default=None)
    icon_id = models.IntegerField(null=True, default=None, db_index=True)
    published = models.BooleanField(null=True, default=None, db_index=True)

    def __str__(self):
        return self.category_name

    class EveSdeMapping:
        table = "invCategories"
        order = 6
        field_mappings = {
            "published": "bool",
        }


class EveConstellation(models.Model):
    region = models.ForeignKey(
        "EveRegion", on_delete=models.CASCADE, null=True, default=None
    )
    constellation_id = models.IntegerField(primary_key=True)
    constellation_name = models.CharField(max_length=100, null=True, default=None)
    x = models.FloatField(null=True, default=None)
    y = models.FloatField(null=True, default=None)
    z = models.FloatField(null=True, default=None)
    x_min = models.FloatField(null=True, default=None)
    x_max = models.FloatField(null=True, default=None)
    y_min = models.FloatField(null=True, default=None)
    y_max = models.FloatField(null=True, default=None)
    z_min = models.FloatField(null=True, default=None)
    z_max = models.FloatField(null=True, default=None)
    faction = models.ForeignKey(
        "EveFaction", on_delete=models.CASCADE, null=True, default=None
    )
    radius = models.FloatField(null=True, default=None)

    def __str__(self):
        return self.constellation_name

    class EveSdeMapping:
        table = "mapConstellations"
        order = 4


class EveFaction(models.Model):
    """SDE table: chrFactions"""

    faction_id = models.IntegerField(primary_key=True)
    faction_name = models.CharField(max_length=100, null=True, default=None)
    description = models.TextField(null=True, default=None)
    # race_id not implemented as FK because some entries not found in chrRaces
    race_id = models.IntegerField(null=True, default=None, db_index=True)
    solar_system_id = models.IntegerField(null=True, default=None, db_index=True)
    corporation_id = models.IntegerField(null=True, default=None, db_index=True)
    size_factor = models.FloatField(null=True, default=None)
    station_count = models.IntegerField(null=True, default=None)
    station_system_count = models.IntegerField(null=True, default=None)
    militia_corporation_id = models.IntegerField(null=True, default=None, db_index=True)
    icon_id = models.IntegerField(null=True, default=None, db_index=True)

    def __str__(self):
        return self.faction_name

    class EveSdeMapping:
        table = "chrFactions"
        order = 2
        name_mappings = {
            "raceIDs": "raceID",
        }


class EveGroup(models.Model):
    """SDE table: invGroups"""

    group_id = models.IntegerField(primary_key=True)
    category = models.ForeignKey(
        "EveCategory", on_delete=models.CASCADE, null=True, default=None
    )
    group_name = models.CharField(max_length=100, null=True, default=None)
    icon_id = models.IntegerField(null=True, default=None, db_index=True)
    use_base_price = models.BooleanField(null=True, default=None)
    anchored = models.BooleanField(null=True, default=None)
    anchorable = models.BooleanField(null=True, default=None)
    fittable_non_singleton = models.BooleanField(null=True, default=None)
    published = models.BooleanField(null=True, default=None, db_index=True)

    def __str__(self):
        return self.group_name

    class EveSdeMapping:
        table = "invGroups"
        order = 6
        field_mappings = {
            "use_base_price": "bool",
            "anchored": "bool",
            "anchorable": "bool",
            "fittable_non_singleton": "bool",
            "published": "bool",
        }


class EveItem(models.Model):
    item_id = models.IntegerField(primary_key=True)
    type = models.ForeignKey(
        "EveType", on_delete=models.CASCADE, null=True, default=None
    )
    # not implemented as FK since updating would not work
    owner_id = models.IntegerField(null=True, default=None, db_index=True)
    # not implemented as FK since updating would not work
    location_id = models.IntegerField(null=True, default=None, db_index=True)
    flag_id = models.IntegerField(null=True, default=None, db_index=True)
    quantity = models.IntegerField(null=True, default=None, db_index=True)

    def __str__(self):
        return str(self.item_id)

    class EveSdeMapping:
        table = "invItems"
        order = 10
        none_mappings = {
            "typeID": "-1",
        }


class EveItemDenormalized(models.Model):
    item = models.OneToOneField(
        "EveItem",
        on_delete=models.CASCADE,
        primary_key=True,
    )
    type = models.ForeignKey(
        "EveType", on_delete=models.CASCADE, null=True, default=None
    )
    group = models.ForeignKey(
        "EveGroup", on_delete=models.CASCADE, null=True, default=None
    )
    solar_system = models.ForeignKey(
        "EveSolarSystem", on_delete=models.CASCADE, null=True, default=None
    )
    constellation = models.ForeignKey(
        "EveConstellation", on_delete=models.CASCADE, null=True, default=None
    )
    region = models.ForeignKey(
        "EveRegion", on_delete=models.CASCADE, null=True, default=None
    )
    orbit_id = models.IntegerField(null=True, default=None)
    x = models.FloatField(null=True, default=None)
    y = models.FloatField(null=True, default=None)
    z = models.FloatField(null=True, default=None)
    radius = models.FloatField(null=True, default=None)
    item_name = models.CharField(max_length=200, null=True, default=None)
    security = models.FloatField(null=True, default=None)
    celestial_index = models.IntegerField(null=True, default=None)
    orbit_index = models.IntegerField(null=True, default=None)

    def __str__(self):
        return "{} - {}".format(self.item_id, self.item_name)

    def get_distance_to(self, x0: float, y0: float, z0: float) -> float:
        """returns distance from this item to given position in meters"""
        return math.sqrt((x0 - self.x) ** 2 + (y0 - self.y) ** 2 + (z0 - self.z) ** 2)

    class EveSdeMapping:
        table = "mapDenormalize"
        order = 11


class EveRace(models.Model):
    race_id = models.IntegerField(primary_key=True)
    race_name = models.CharField(max_length=100, null=True, default=None)
    description = models.TextField(null=True, default=None)
    icon_id = models.IntegerField(null=True, default=None, db_index=True)
    short_description = models.TextField(null=True, default=None)

    def __str__(self):
        return self.race_name

    class EveSdeMapping:
        table = "chrRaces"
        order = 1


class EveRegion(models.Model):
    region_id = models.IntegerField(primary_key=True)
    region_name = models.CharField(max_length=100, null=True, default=None)
    x = models.FloatField(null=True, default=None)
    y = models.FloatField(null=True, default=None)
    z = models.FloatField(null=True, default=None)
    x_min = models.FloatField(null=True, default=None)
    x_max = models.FloatField(null=True, default=None)
    y_min = models.FloatField(null=True, default=None)
    y_max = models.FloatField(null=True, default=None)
    z_min = models.FloatField(null=True, default=None)
    z_max = models.FloatField(null=True, default=None)
    faction = models.ForeignKey(
        "EveFaction", on_delete=models.CASCADE, null=True, default=None
    )
    radius = models.FloatField(null=True, default=None)

    def __str__(self):
        return self.region_name

    class EveSdeMapping:
        table = "mapRegions"
        order = 3


class EveSolarSystem(models.Model):
    region = models.ForeignKey(
        "EveRegion", on_delete=models.CASCADE, null=True, default=None
    )
    constellation = models.ForeignKey(
        "EveConstellation", on_delete=models.CASCADE, null=True, default=None
    )
    solar_system_id = models.IntegerField(primary_key=True)
    solar_system_name = models.CharField(max_length=100, null=True, default=None)
    x = models.FloatField(null=True, default=None)
    y = models.FloatField(null=True, default=None)
    z = models.FloatField(null=True, default=None)
    x_min = models.FloatField(null=True, default=None)
    x_max = models.FloatField(null=True, default=None)
    y_min = models.FloatField(null=True, default=None)
    y_max = models.FloatField(null=True, default=None)
    z_min = models.FloatField(null=True, default=None)
    z_max = models.FloatField(null=True, default=None)
    luminosity = models.FloatField(null=True, default=None)
    border = models.BooleanField(null=True, default=None)
    fringe = models.BooleanField(null=True, default=None)
    corridor = models.BooleanField(null=True, default=None)
    hub = models.BooleanField(null=True, default=None)
    international = models.BooleanField(null=True, default=None)
    # renamed from region to confirm with constellation renameing
    regional_gate = models.BooleanField(null=True, default=None)
    # renamed from constellation to avoid nameing conflict with Fk field
    constellation_gate = models.BooleanField(null=True, default=None)
    security = models.FloatField(null=True, default=None)
    faction = models.ForeignKey(
        "EveFaction", on_delete=models.CASCADE, null=True, default=None
    )
    radius = models.FloatField(null=True, default=None)
    sun_type_id = models.IntegerField(null=True, default=None, db_index=True)
    security_class = models.CharField(max_length=2, null=True, default=None)

    def __str__(self):
        return self.solar_system_name

    def get_celestials_by_distance(
        self, x0: float, y0: float, z0: float, group_id: int = None
    ) -> list:
        """get celestials by distance to a position in this solar solar_system
        Args:
            x, y, z: reference position
            group_id: (optional) limit celestials to members of an EveGroup

        Returns:
            list of celestials with distance to position in ascending order
        """
        if group_id is None:
            celestials = EveItemDenormalized.objects.filter(solar_system=self)
        else:
            celestials = EveItemDenormalized.objects.filter(
                solar_system=self, group_id=group_id
            )
        celestials_distance = [
            {"celestial": c, "distance": c.get_distance_to(x0, y0, z0)}
            for c in celestials
        ]
        return sorted(celestials_distance, key=lambda x: x["distance"])

    def get_nearest_celestials(
        self,
        x0: float,
        y0: float,
        z0: float,
        group_id: int = None,
        max_distance: int = None,
    ):
        """returns the nearest celestial to a given position

        Args:
            x, y, z: reference position
            group_id: (optional) limit celestials to members of an EveGroup
            max_distance: (optional) max valid distance for "near" in meters

        Returns:
            list of celestials with distance to position in ascending order

        """
        celestials = self.get_celestials_by_distance(x0, y0, z0, group_id)
        if celestials:
            first = celestials[0]["celestial"]
            if not max_distance or celestials[0]["distance"] <= max_distance:
                item = first
            else:
                item = None
        else:
            item = None

        return item

    class EveSdeMapping:
        table = "mapSolarSystems"
        order = 5
        name_mappings = {
            "regional": "regionalGate",
            "constellation": "constellationGate",
        }
        field_mappings = {
            "border": "bool",
            "fringe": "bool",
            "corridor": "bool",
            "hub": "bool",
            "international": "bool",
            "regional": "bool",
            "constellation": "bool",
        }


class EveType(models.Model):
    type_id = models.IntegerField(primary_key=True)
    group = models.ForeignKey(
        "EveGroup", on_delete=models.CASCADE, null=True, default=None
    )
    type_name = models.CharField(max_length=100, null=True, default=None)
    description = models.TextField(null=True, default=None)
    mass = models.FloatField(null=True, default=None)
    volume = models.FloatField(null=True, default=None)
    capacity = models.FloatField(null=True, default=None)
    portion_size = models.IntegerField(null=True, default=None)
    # race_id not implemented as FK because some entries not found in chrRaces
    race_id = models.IntegerField(null=True, default=None, db_index=True)
    base_price = models.FloatField(null=True, default=None)
    published = models.BooleanField(null=True, default=None, db_index=True)
    market_group_id = models.IntegerField(null=True, default=None, db_index=True)
    icon_id = models.IntegerField(null=True, default=None, db_index=True)
    sound_id = models.IntegerField(null=True, default=None, db_index=True)
    graphic_id = models.IntegerField(null=True, default=None, db_index=True)

    def __str__(self):
        return self.type_name

    class EveSdeMapping:
        table = "invTypes"
        order = 7
        field_mappings = {
            "published": "bool",
        }


class EveTypeMaterial(models.Model):
    type = models.ForeignKey(
        "EveType", on_delete=models.CASCADE, related_name="parent_type"
    )
    material_type = models.ForeignKey("EveType", on_delete=models.CASCADE)
    quantity = models.IntegerField()

    class Meta:
        unique_together = (("type", "material_type"),)
        indexes = [
            models.Index(fields=["type"]),
        ]

    def __str__(self):
        return "{} - {}".format(self.type.type_name, self.material_type.type_name)

    class EveSdeMapping:
        table = "invTypeMaterials"
        order = 9
        pk_columns = ["type_id", "material_type_id"]
