# Generated by Django 2.2.5 on 2019-09-27 12:21

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("evesde", "0008_auto_20190915_1844"),
    ]

    operations = [
        migrations.CreateModel(
            name="EveTypeMaterial",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("quantity", models.IntegerField()),
                (
                    "material_type",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="evesde.EveType"
                    ),
                ),
                (
                    "type",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="parent_type",
                        to="evesde.EveType",
                    ),
                ),
            ],
        ),
        migrations.AddConstraint(
            model_name="evetypematerial",
            constraint=models.UniqueConstraint(
                fields=("type", "material_type"), name="functional_pk"
            ),
        ),
    ]
