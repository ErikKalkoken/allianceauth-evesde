# Generated by Django 2.2.5 on 2019-09-12 17:43

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="EveCategory",
            fields=[
                ("category_id", models.IntegerField(primary_key=True, serialize=False)),
                (
                    "category_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("icon_id", models.IntegerField(default=None, null=True)),
                ("published", models.BooleanField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name="EveConstellation",
            fields=[
                (
                    "constellation_id",
                    models.IntegerField(primary_key=True, serialize=False),
                ),
                (
                    "constellation_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("x", models.FloatField(default=None, null=True)),
                ("y", models.FloatField(default=None, null=True)),
                ("z", models.FloatField(default=None, null=True)),
                ("x_min", models.FloatField(default=None, null=True)),
                ("x_max", models.FloatField(default=None, null=True)),
                ("y_min", models.FloatField(default=None, null=True)),
                ("y_max", models.FloatField(default=None, null=True)),
                ("z_min", models.FloatField(default=None, null=True)),
                ("z_max", models.FloatField(default=None, null=True)),
                ("radius", models.FloatField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name="EveFaction",
            fields=[
                ("faction_id", models.IntegerField(primary_key=True, serialize=False)),
                (
                    "faction_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("description", models.TextField(default=None, null=True)),
                ("race_id", models.IntegerField(default=None, null=True)),
                ("solar_system_id", models.IntegerField(default=None, null=True)),
                ("corporation_id", models.IntegerField(default=None, null=True)),
                ("size_factor", models.FloatField(default=None, null=True)),
                ("station_count", models.IntegerField(default=None, null=True)),
                ("station_system_count", models.IntegerField(default=None, null=True)),
                (
                    "militia_corporation_id",
                    models.IntegerField(default=None, null=True),
                ),
                ("icon_id", models.IntegerField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name="EveGroup",
            fields=[
                ("group_id", models.IntegerField(primary_key=True, serialize=False)),
                (
                    "group_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("icon_id", models.IntegerField(default=None, null=True)),
                ("use_base_price", models.BooleanField(default=None, null=True)),
                ("anchored", models.BooleanField(default=None, null=True)),
                ("anchorable", models.BooleanField(default=None, null=True)),
                (
                    "fittable_non_singleton",
                    models.BooleanField(default=None, null=True),
                ),
                ("published", models.BooleanField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name="EveItem",
            fields=[
                ("item_id", models.IntegerField(primary_key=True, serialize=False)),
                ("owner_id", models.IntegerField(default=None, null=True)),
                ("location_id", models.IntegerField(default=None, null=True)),
                ("flag_id", models.IntegerField(default=None, null=True)),
                ("quantity", models.IntegerField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name="EveRace",
            fields=[
                ("race_id", models.IntegerField(primary_key=True, serialize=False)),
                (
                    "race_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("description", models.TextField(default=None, null=True)),
                ("icon_id", models.IntegerField(default=None, null=True)),
                ("short_description", models.TextField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name="EveRegion",
            fields=[
                ("region_id", models.IntegerField(primary_key=True, serialize=False)),
                (
                    "region_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("x", models.FloatField(default=None, null=True)),
                ("y", models.FloatField(default=None, null=True)),
                ("z", models.FloatField(default=None, null=True)),
                ("x_min", models.FloatField(default=None, null=True)),
                ("x_max", models.FloatField(default=None, null=True)),
                ("y_min", models.FloatField(default=None, null=True)),
                ("y_max", models.FloatField(default=None, null=True)),
                ("z_min", models.FloatField(default=None, null=True)),
                ("z_max", models.FloatField(default=None, null=True)),
                ("radius", models.FloatField(default=None, null=True)),
                (
                    "faction",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="evesde.EveFaction",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="EveName",
            fields=[
                (
                    "item",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        primary_key=True,
                        serialize=False,
                        to="evesde.EveItem",
                    ),
                ),
                ("item_name", models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name="EveType",
            fields=[
                ("type_id", models.IntegerField(primary_key=True, serialize=False)),
                (
                    "type_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("description", models.TextField(default=None, null=True)),
                ("mass", models.FloatField(default=None, null=True)),
                ("volume", models.FloatField(default=None, null=True)),
                ("capacity", models.FloatField(default=None, null=True)),
                ("portion_size", models.IntegerField(default=None, null=True)),
                ("race_id", models.IntegerField(default=None, null=True)),
                ("base_price", models.FloatField(default=None, null=True)),
                ("published", models.BooleanField(default=None, null=True)),
                ("market_group_id", models.IntegerField(default=None, null=True)),
                ("icon_id", models.IntegerField(default=None, null=True)),
                ("sound_id", models.IntegerField(default=None, null=True)),
                ("graphic_id", models.IntegerField(default=None, null=True)),
                (
                    "group",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="evesde.EveGroup",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="EveSolarSystem",
            fields=[
                (
                    "solar_system_id",
                    models.IntegerField(primary_key=True, serialize=False),
                ),
                (
                    "solar_system_name",
                    models.CharField(default=None, max_length=100, null=True),
                ),
                ("x", models.FloatField(default=None, null=True)),
                ("y", models.FloatField(default=None, null=True)),
                ("z", models.FloatField(default=None, null=True)),
                ("x_min", models.FloatField(default=None, null=True)),
                ("x_max", models.FloatField(default=None, null=True)),
                ("y_min", models.FloatField(default=None, null=True)),
                ("y_max", models.FloatField(default=None, null=True)),
                ("z_min", models.FloatField(default=None, null=True)),
                ("z_max", models.FloatField(default=None, null=True)),
                ("luminosity", models.FloatField(default=None, null=True)),
                ("border", models.BooleanField(default=None, null=True)),
                ("fringe", models.BooleanField(default=None, null=True)),
                ("corridor", models.BooleanField(default=None, null=True)),
                ("hub", models.BooleanField(default=None, null=True)),
                ("international", models.BooleanField(default=None, null=True)),
                ("regional_gate", models.BooleanField(default=None, null=True)),
                ("constellation_gate", models.BooleanField(default=None, null=True)),
                ("security", models.FloatField(default=None, null=True)),
                ("radius", models.FloatField(default=None, null=True)),
                ("sun_type_id", models.IntegerField(default=None, null=True)),
                (
                    "security_class",
                    models.CharField(default=None, max_length=2, null=True),
                ),
                (
                    "constellation",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="evesde.EveConstellation",
                    ),
                ),
                (
                    "faction",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="evesde.EveFaction",
                    ),
                ),
                (
                    "region",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="evesde.EveRegion",
                    ),
                ),
            ],
        ),
        migrations.AddIndex(
            model_name="everace",
            index=models.Index(
                fields=["icon_id"], name="evesde_ever_icon_id_a50107_idx"
            ),
        ),
        migrations.AddField(
            model_name="eveitem",
            name="type",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="evesde.EveType",
            ),
        ),
        migrations.AddField(
            model_name="evegroup",
            name="category",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="evesde.EveCategory",
            ),
        ),
        migrations.AddIndex(
            model_name="evefaction",
            index=models.Index(
                fields=["race_id"], name="evesde_evef_race_id_c54d1b_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evefaction",
            index=models.Index(
                fields=["solar_system_id"], name="evesde_evef_solar_s_8ec634_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evefaction",
            index=models.Index(
                fields=["corporation_id"], name="evesde_evef_corpora_d5a9fd_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evefaction",
            index=models.Index(
                fields=["militia_corporation_id"], name="evesde_evef_militia_1dac05_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evefaction",
            index=models.Index(
                fields=["icon_id"], name="evesde_evef_icon_id_a7f126_idx"
            ),
        ),
        migrations.AddField(
            model_name="eveconstellation",
            name="faction",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="evesde.EveFaction",
            ),
        ),
        migrations.AddField(
            model_name="eveconstellation",
            name="region",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="evesde.EveRegion",
            ),
        ),
        migrations.AddIndex(
            model_name="evecategory",
            index=models.Index(
                fields=["icon_id"], name="evesde_evec_icon_id_2a0467_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evecategory",
            index=models.Index(
                fields=["published"], name="evesde_evec_publish_6816ae_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evetype",
            index=models.Index(
                fields=["race_id"], name="evesde_evet_race_id_b81885_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evetype",
            index=models.Index(
                fields=["published"], name="evesde_evet_publish_860a06_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evetype",
            index=models.Index(
                fields=["market_group_id"], name="evesde_evet_market__9fde97_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evetype",
            index=models.Index(
                fields=["icon_id"], name="evesde_evet_icon_id_7c2e26_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evetype",
            index=models.Index(
                fields=["sound_id"], name="evesde_evet_sound_i_e70a4e_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evetype",
            index=models.Index(
                fields=["graphic_id"], name="evesde_evet_graphic_b7ce8b_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evesolarsystem",
            index=models.Index(
                fields=["sun_type_id"], name="evesde_eves_sun_typ_b40d0d_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="eveitem",
            index=models.Index(
                fields=["flag_id"], name="evesde_evei_flag_id_9bc099_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="eveitem",
            index=models.Index(
                fields=["owner_id"], name="evesde_evei_owner_i_d2f15d_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evegroup",
            index=models.Index(
                fields=["icon_id"], name="evesde_eveg_icon_id_762838_idx"
            ),
        ),
        migrations.AddIndex(
            model_name="evegroup",
            index=models.Index(
                fields=["published"], name="evesde_eveg_publish_986a85_idx"
            ),
        ),
    ]
