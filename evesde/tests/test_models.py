from django.test import TestCase
from django.utils.timezone import now

from ..models import UpdateStatus
from ..utils import set_test_logger

MODULE_PATH = "evesde.models"
logger = set_test_logger(MODULE_PATH, __file__)


class TestUpdateStatus(TestCase):
    def test_str(self):
        x = UpdateStatus(
            model_name="EveSolarSystem", last_modified=now(), last_updated=now()
        )
        self.assertEqual(str(x), "EveSolarSystem")


class TestEveSolarSystem(TestCase):

    pass
