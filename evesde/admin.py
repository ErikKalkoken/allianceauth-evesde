from django.contrib import admin

from .models import *  # noqa
from .models import UpdateStatus
from .tasks import run_update


@admin.register(UpdateStatus)
class UpdateStatusAdmin(admin.ModelAdmin):
    list_display = (
        "model_name",
        "sde_table_name",
        "load_order",
        "items_count",
        "last_modified",
        "last_updated",
    )
    actions = [
        "start_update_single",
        "start_update_single_forced",
        "start_update_all",
        "start_update_all_forced",
    ]
    list_display_links = None

    def my_dummy(self, obj):
        return obj.dummy

    def sde_table_name(self, obj):
        """returns the name of the SDE table of this model"""
        try:
            c = eval(obj.model_name)
            result = c.EveSdeMapping.table
        except NameError:
            result = None
        return result

    def load_order(self, obj):
        """returns load order of this model"""
        try:
            c = eval(obj.model_name)
            result = c.EveSdeMapping.order
        except NameError:
            result = None
        return result

    def items_count(self, obj):
        """returns item count for given model"""
        try:
            c = eval(obj.model_name)
            result = "{:,}".format(c.objects.count())
        except NameError:
            result = None
        return result

    def _start_update_single(self, request, queryset, force_update=False):
        if len(queryset) > 1:
            self.message_user(
                request, "Please select no more than one model.", "warning"
            )
        else:
            model = queryset.first()
            run_update.delay(
                model_to_update=model.model_name, force_update=force_update
            )
            self.message_user(
                request,
                "Started {} update for model {}. You will receive "
                "a notification once the process is complete.".format(
                    "forced" if force_update else "regular", model.model_name
                ),
            )

    def start_update_single(self, request, queryset):
        self._start_update_single(request, queryset)

    start_update_single.short_description = "Run regular update for selected model"

    def start_update_single_forced(self, request, queryset):
        self._start_update_single(request, queryset, True)

    start_update_single_forced.short_description = (
        "Run forced update for selected model"
    )

    def start_update_all(self, request, queryset):
        run_update.delay(force_update=False)
        self.message_user(
            request,
            "Started regular update process for all models. "
            "You will receive a notification once the process is complete",
        )

    start_update_all.short_description = "Run regular update for all models"

    def start_update_all_forced(self, request, queryset):
        run_update.delay(force_update=True)
        self.message_user(
            request,
            "Started forced update process for all models. "
            "You will receive a notification once the process is complete",
        )

    start_update_all_forced.short_description = "Run forced update for all models"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
