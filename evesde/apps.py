from django.apps import AppConfig

from . import __version__


class EveSDEConfig(AppConfig):
    name = "evesde"
    label = "evesde"
    verbose_name = "Eve Online Static Data v{}".format(__version__)
